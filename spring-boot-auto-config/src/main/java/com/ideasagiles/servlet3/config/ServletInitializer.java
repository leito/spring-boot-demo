package com.ideasagiles.servlet3.config;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;

/**
 * Esta clase se encarga de levantar el contexto de Spring Boot.
 * A partir de esta clase se configura Spring Boot y sus dependencias.
 */
public class ServletInitializer extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(AppConfig.class);
    }

}
