<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Pagina de Login</title>
    </head>
    <body>
        <h1>Pagina de Login</h1>
        <c:if test="${param.error != null}">
            Nombre de usuario o password incorrecto.
        </c:if>
        <form method="POST" action="<c:url value="/login"/>">
            Usuario: <input type="text" name="username" />
            <br/>
            Password: <input type="password" name="password" />
            <br/>
            <input type="submit" value="Login" />
        </form>
    </body>
</html>
